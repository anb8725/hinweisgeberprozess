package edu.thi.beans;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

//Autor: Fabian Genes
@Stateless
@LocalBean
public class VerifikationDerMeldungServiceBean implements VerifikationDerMeldungBeanLocal {
    @Inject
    AMQServiceBean amqServiceBean;

    //Die Email und die Gewollte Nachfrage
    //werden im JSON Format an ActiveMQ geschickt
    public void sendAdaptationRequestToAMQ(String adaptationRequest_queue, String email, String nachfragen) {
        String jsonString = "{\"email\":\"" + email + "\", \n" +
                "\"nachfragen\":\"" + nachfragen + "\" \n"
                + " }";
        amqServiceBean.sendMessageToAMQ(adaptationRequest_queue, jsonString);
    }

}
