package edu.thi.beans;

import edu.thi.entity.PublicMessage;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;

/*
 * author: Christina Huber
 * */
@Stateless
@LocalBean
public class KommunikationskanalueberwachungServiceBean implements KommunikationsueberwachungBeanLocal {
    @Inject
    AMQServiceBean amqServiceBean;

    /* Description:
     * this method adds messages to a list. if the messagelist is empty it generates a new one.
     * */
    public ArrayList<PublicMessage> addMessageToList(ArrayList<PublicMessage> messageList, Long ticketId, String source, String category, String comment) {

        PublicMessage message = new PublicMessage(ticketId, source, category, comment);

        if (messageList == null)  // messageMap gibt es noch nicht
            messageList = new ArrayList<PublicMessage>(); //dann angelegt

        messageList.add(message); // falls es schon existiert angehängt

        return messageList;

    }

    /*Description:
     * this methods stringifys the messageList to sends it to the Message_Qeue
     * */
    public void storeMessageList(String Message_Queue, ArrayList<PublicMessage> messageList,long ticketID) {

        StringBuffer sb = new StringBuffer();
        sb.append("{\n");
        sb.append("\t\"ticketID\": \""+ticketID+ "\",\n");
        sb.append("\t\"messages\":[\n");

        for (int i = 0; i < messageList.size(); i++) {
            sb.append("\t{\n");
            sb.append("\t\t\"messageID\": \"" + messageList.get(i).getMessageid() + "\",\n");
            sb.append("\t\t\"category\": \"" + messageList.get(i).getCategory() + "\",\n");
            sb.append("\t\t\"source\": \"" + messageList.get(i).getSource() + "\",\n");
            sb.append("\t\t\"comment\": \"" + messageList.get(i).getComment() + "\"\n");
            if (i < messageList.size()-1) {
                sb.append("\t},\n");
            } else {
                sb.append("\t}\n"); // last element of json array does not have a ","
            }
        }
        sb.append("]}");
        amqServiceBean.sendMessageToAMQ(Message_Queue, sb.toString());
    }


}


