package edu.thi.beans;


import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.MessageCorrelationResult;
import org.camunda.bpm.engine.runtime.ProcessInstance;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

//Autor: Anke Brenner
@Stateless
@LocalBean
public class PersonenschutzEinleitenServiceBean implements PersonenschutzEinleitenBeanLocal {
    @Inject
    AMQServiceBean amqServiceBean;

    //eine Personenschutzanfrage wird über Korrelation an den nachfolgenden Prozess geschickt
    public void sendPersonalProtectionRequest(String levelOfExplosiveness, String effectsOnCompany, String complaint, RuntimeService runtimeService, String username, String company, String processInstanceId) {
        String levelOfExplosivenessString = (String) levelOfExplosiveness;

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("company", company);
        data.put("complaint", complaint);
        data.put("levelOfExplosivenessString", levelOfExplosivenessString);
        data.put("effectsOnCompany", effectsOnCompany);
        data.put("username", username);
        data.put("correlationInstanceId", processInstanceId);

        MessageCorrelationResult mcresult = runtimeService.createMessageCorrelation("Personenschutzanfrage")
                .setVariables(data)
                .correlateWithResult();

        ProcessInstance processInstance = mcresult.getProcessInstance();
        System.out.println("New process with ProcessInstanceId " + processInstance.getProcessInstanceId() + " was started!");

    }

    //die Daten einer Person,
    // die ins Personenschutzprogramm aufgenommen werden soll,
    // werden im JSON Format an ActiveMQ geschickt
    public void includePersonIntoProtectionProgram(String username) {

        //create JSON for message
        StringBuffer sb = new StringBuffer();
        sb.append("{\n");
        sb.append("\t\"username\": \"" + username + "\" \n");
        sb.append("}\n");
        amqServiceBean.sendMessageToAMQ("Add_Person_To_Program", sb.toString());
    }

    //Bestätigung für Aufnahme ins Personenschutzprogramm wird über Korrelation versendet
    public void sendPersonalProtectionAffirmation(RuntimeService runtimeService, String correlationInstanceId) {
        runtimeService.createMessageCorrelation("Bestaetigung")
                .processInstanceVariableEquals("processInstanceId", correlationInstanceId)
                .correlateWithResult();

        System.out.println("Bestaetigung wurde versendet");
    }

    //Ablehnung für Aufnahme ins Personenschutzprogramm wird über Korrelation versendet
    public void sendPersonalProtectionRejection(RuntimeService runtimeService, String correlationInstanceId) {
        MessageCorrelationResult mcresult = runtimeService.createMessageCorrelation("Ablehnung")
                .processInstanceVariableEquals("processInstanceId", correlationInstanceId)
                .correlateWithResult();

        System.out.println("Ablehnung wurde versendet");
    }
}
