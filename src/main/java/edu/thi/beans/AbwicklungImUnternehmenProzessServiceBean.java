package edu.thi.beans;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

/*
 * author: Christina Huber
 * */
@Stateless
@LocalBean
public class AbwicklungImUnternehmenProzessServiceBean implements HinweisgeberprozessBeanLocal {
    @Inject
    AMQServiceBean amqServiceBean;

    /*
     * description: this method sends the employeeinformation to the Employee_Queue
     * */
    public void informAllAffectedEmployees(String Employee_Queue, String affectedCompanyArea, String information, long ticketID) {

        StringBuffer sb = new StringBuffer();
        sb.append("{\n");
        sb.append("\t\"level\": \"" + affectedCompanyArea + "\",\n");
        sb.append("\t\"ticketID\": \"" + ticketID + "\",\n");
        sb.append("\t\"employeeInformation\": \"" + information + "\"\n");
        sb.append("}");
        amqServiceBean.sendMessageToAMQ(Employee_Queue, sb.toString());
    }

}


