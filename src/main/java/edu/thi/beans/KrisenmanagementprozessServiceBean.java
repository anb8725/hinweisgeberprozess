package edu.thi.beans;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;


@Stateless
@LocalBean
/*
 * author: Christina Huber
 * description: this bean builds a json valid string and hands it over to amqservicebean to get pushed on Press Queue
 * */
public class KrisenmanagementprozessServiceBean implements HinweisgeberprozessBeanLocal {
    @Inject
    AMQServiceBean amqServiceBean;

    public void sendPressStatementToPublic(String Press_Queue, String pressStatement, String category, long ticketID) {
        StringBuffer sb = new StringBuffer();
        sb.append("{\n");
        sb.append("\t\"category\": \"" + category + "\",\n");
        sb.append("\t\"ticketID\": \"" + ticketID + "\",\n");
        sb.append("\t\"body\": \"" + pressStatement + "\"\n");
        sb.append("}");
        amqServiceBean.sendMessageToAMQ(Press_Queue, sb.toString());
    }


}




