package edu.thi.beans;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jms.*;

//Autor: Fabian Genes
@Stateless
@LocalBean
public class AMQServiceBean implements AMQBeanLocal {

    //Methode zum Senden an die Active Message Queue
    //Nur funktional wenn Camunda im Docker Container läuft
    public void sendMessageToAMQ(String queueName, String messageString) {
        String user = ActiveMQConnection.DEFAULT_USER;
        String password = ActiveMQConnection.DEFAULT_PASSWORD;
        String url = ActiveMQConnection.DEFAULT_BROKER_URL;
        Destination destination;

        try {
            Connection connection = null;
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER, ActiveMQConnection.DEFAULT_PASSWORD, ActiveMQConnection.DEFAULT_BROKER_URL.replace("localhost", "activemq"));
            connection = connectionFactory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            destination = session.createQueue(queueName);
            MessageProducer producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            TextMessage message = session.createTextMessage(messageString);
            producer.send(message);

            connection.close();

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }


}
