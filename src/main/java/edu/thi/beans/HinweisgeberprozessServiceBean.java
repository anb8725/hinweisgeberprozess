package edu.thi.beans;

import org.json.JSONObject;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

//Autor: Fabian Genes
@Stateless
@LocalBean
public class HinweisgeberprozessServiceBean implements HinweisgeberprozessBeanLocal {
    @Inject
    AMQServiceBean amqServiceBean;

    //Der Username
    //wird im JSON Format an die ActiveMQ geschickt
    public void sendUsernameToAMQ(String queueName, String username) {
        String jsonString = "{\"username\":\"" + username + "\" }";
        amqServiceBean.sendMessageToAMQ(queueName, jsonString);
    }


    //Hier wird über einen POST REST Aufruf die Daten eines Tickets, als JSON im Body, an den Endpunkt "http://host.docker.internal:9090/ticket" gesendet.
    //Der POST Aufruf liefert die ID des am Endpunkt erstellten Tickets zurück und gibt diese ID zurück
    public Long createTicket(String username, String company, String complain) throws IOException {


        URL url = new URL("http://host.docker.internal:9090/ticket");
        HttpURLConnection http = (HttpURLConnection) url.openConnection();
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        http.setDoInput(true);
        http.setRequestProperty("Accept", "application/json");
        http.setRequestProperty("Content-Type", "application/json");

        String message =
                "{" + "\n" +

                        "\t\t" + "\"username\":\"" + username + "\",\n" +
                        "\t\t" + "\"company\":\"" + company + "\",\n" +
                        "\t\t" + "\"complain\":\"" + complain + "\"\n" +
                        "\n}";

        byte[] out = message.getBytes(StandardCharsets.UTF_8);

        OutputStream stream = http.getOutputStream();
        stream.write(out);

        String response = new String(http.getInputStream().readAllBytes(), StandardCharsets.UTF_8);
        http.disconnect();

        JSONObject jsonObject = new JSONObject(response);
        return jsonObject.getLong("id");
    }

    //Die ticketID wird im JSON Format an die ActiveMQ geschickt
    public void sendTicketIdToAMQ(String updateIsExplosiveTicket_queue, long ticketID) {
        String jsonString = "{\"ticketID\":\"" + ticketID + "\" }";
        amqServiceBean.sendMessageToAMQ(updateIsExplosiveTicket_queue, jsonString);
    }

    //username,company, complaint, ticketID, strategy, problemFixed wird im JSON Format an die ActiveMQ geschickt

    public void createOverallReportAMQ(String createOverallReport_queue, String username, String company, String complaint, Long ticketID, String strategy, String problemFixed) {
        String message =
                "{" + "\n" +

                        "\t\t" + "\"username\":\"" + username + "\",\n" +
                        "\t\t" + "\"company\":\"" + company + "\",\n" +
                        "\t\t" + "\"complain\":\"" + complaint + "\",\n" +
                        "\t\t" + "\"strategy\":\"" + strategy + "\",\n" +
                        "\t\t" + "\"problemFixed\":\"" + problemFixed + "\",\n" +
                        "\t\t" + "\"ticketID\":\"" + ticketID + "\"\n" +
                        "\n}";
        amqServiceBean.sendMessageToAMQ(createOverallReport_queue, message);


    }

    //Die email wird im JSON Format an die ActiveMQ geschickt
    public void sendTicketGotCreatedToAMQ(String ticketGotCreated_queue, String email) {
        String jsonString = "{\"email\":\"" + email + "\" }";
        amqServiceBean.sendMessageToAMQ(ticketGotCreated_queue, jsonString);
    }

    //Die email wird im JSON Format an die ActiveMQ geschickt
    public void sendReceivedConfirmationToAMQ(String confirmationOfReceipt_queue, String email) {
        String jsonString = "{\"email\":\"" + email + "\" }";
        amqServiceBean.sendMessageToAMQ(confirmationOfReceipt_queue, jsonString);
    }

    //Die email wird im JSON Format an die ActiveMQ geschickt
    public void sendTicketGotCanceledToAMQ(String cancelTicket_queue, String email) {
        String jsonString = "{\"email\":\"" + email + "\" }";
        amqServiceBean.sendMessageToAMQ(cancelTicket_queue, jsonString);
    }

    //Die ticketID und protectionMethods wird im JSON Format an die ActiveMQ geschickt
    public void sendSetTicketIsExplosiveToAMQ(String updateIsExplosiveTicket_queue, long ticketID, String protectionMethods) {
        String jsonString =
                "{" + "\n" +
                        "\t\t" + "\"ticketID\":\"" + ticketID + "\",\n" +
                        "\t\t" + "\"protectionMethods\":\"" + protectionMethods + "\"\n" +
                        "\n}";
        amqServiceBean.sendMessageToAMQ(updateIsExplosiveTicket_queue, jsonString);
    }

    //Die ticketID und problemFixed wird im XML Format an die ActiveMQ geschickt
    public void sendCreateInternActionsReportToAMQ(String createInternActions_queue, String problemFixed, long ticketID) {
        // Send as XML
        String xmlString =
                "<InternActionsReport>\n" +
                        "<ticketID>" + ticketID + "</ticketID>" + "\n" +
                        "<problemFixed>" + problemFixed + "</problemFixed>\n" +
                        "</InternActionsReport>";
        amqServiceBean.sendMessageToAMQ(createInternActions_queue, xmlString);
    }

    //Die ticketID und strategy wird im JSON Format an die ActiveMQ geschickt
    public void sendCreateCrisisManagementReportToAMQ(String createInternActions_queue, long ticketID, String strategy) {
        // Send as JSON
        String jsonString =
                "{" + "\n" +
                        "\t\t" + "\"ticketID\":\"" + ticketID + "\",\n" +
                        "\t\t" + "\"strategy\":\"" + strategy + "\"\n" +
                        "\n}";
        amqServiceBean.sendMessageToAMQ(createInternActions_queue, jsonString);
    }
}
