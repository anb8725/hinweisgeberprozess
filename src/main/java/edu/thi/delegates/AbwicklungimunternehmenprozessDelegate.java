package edu.thi.delegates;

import edu.thi.beans.AbwicklungImUnternehmenProzessServiceBean;
import org.camunda.bpm.engine.delegate.DelegateExecution;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

@Stateless
@LocalBean
@Named
/*
 * author:Christina Huber
 * */
public class AbwicklungimunternehmenprozessDelegate implements AbwicklungimunternehmenprozessDelegateLocal {

    @Inject
    AbwicklungImUnternehmenProzessServiceBean abwicklungImUnternehmenProzessServiceBean;

    // this method gets the necessary informations from camunda and hands it over to the service bean
    public void informAllAffectedEmployees(DelegateExecution execution) throws Exception {

        String level = (String) execution.getVariable("affectedCompanyArea");
        //Get content for message to Employees
        String employeeInformation = (String) execution.getVariable("information");
        long ticketID = (Long) execution.getVariable("ticketID");
        abwicklungImUnternehmenProzessServiceBean.informAllAffectedEmployees("Employee_Queue", level, employeeInformation , ticketID);

    }


}




