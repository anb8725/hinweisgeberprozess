package edu.thi.delegates;

import edu.thi.beans.HinweisgeberprozessServiceBean;
import edu.thi.beans.PersonenschutzEinleitenServiceBean;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.runtime.MessageCorrelationResult;
import org.camunda.bpm.engine.runtime.ProcessInstance;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.HashMap;
import java.util.Map;

//Autor: Anke Brenner
@Stateless
@LocalBean
@Named
public class PersonenschutzEinleitenDelegate implements PersonenschutzEinleitenDelegateLocal{
    @Inject
    PersonenschutzEinleitenServiceBean personenschutzEinleitenServiceBean;

    //Daten für die Personenschutzanfrage werden aus der execution gelesen und an die ServiceBean weitergegeben
    public void sendPersonalProtectionRequest(DelegateExecution execution) throws Exception {

        String company = (String) execution.getVariable("company");
        String username = (String) execution.getVariable("username");

        String levelOfExplosiveness = (String) execution.getVariableTyped("levelOfExplosiveness").getValue().toString();
        String effectsOnCompany = (String) execution.getVariable("effectsOnCompany");
        String complaint = (String) execution.getVariable("complaint");
        String processInstanceId = execution.getProcessInstanceId();
        execution.setVariable("processInstanceId", processInstanceId);
        RuntimeService runtimeService = execution.getProcessEngineServices().getRuntimeService();

        personenschutzEinleitenServiceBean.sendPersonalProtectionRequest(levelOfExplosiveness, effectsOnCompany, complaint, runtimeService,username,company, processInstanceId);
    }

    // der username wird aus der execution gelesen und an die ServiceBean weitergegeben
    public void includePersonIntoProtectionProgram (DelegateExecution execution) throws Exception {
        String username = (String) execution.getVariable("username");

        personenschutzEinleitenServiceBean.includePersonIntoProtectionProgram(username);
    }

    //runtimeService wird aus der execution gelesen und an die ServiceBean weitergegeben
    public void sendPersonalProtectionAffirmation (DelegateExecution execution) throws Exception {

        String correlationInstanceId = (String) execution.getVariable("correlationInstanceId");
        RuntimeService runtimeService = execution.getProcessEngineServices().getRuntimeService();

        personenschutzEinleitenServiceBean.sendPersonalProtectionAffirmation(runtimeService, correlationInstanceId);
    }

    //runtimeService wird aus der execution gelesen und an die ServiceBean weitergegeben
    public void sendPersonalProtectionRejection (DelegateExecution execution) throws Exception {

        String correlationInstanceId = (String) execution.getVariable("correlationInstanceId");
        RuntimeService runtimeService = execution.getProcessEngineServices().getRuntimeService();

        personenschutzEinleitenServiceBean.sendPersonalProtectionRejection(runtimeService, correlationInstanceId);
    }


}
