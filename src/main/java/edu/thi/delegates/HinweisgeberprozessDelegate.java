package edu.thi.delegates;

import edu.thi.beans.HinweisgeberprozessServiceBean;
import org.camunda.bpm.engine.delegate.DelegateExecution;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

//Autor: Fabian Genes
@Stateless
@LocalBean
@Named
public class HinweisgeberprozessDelegate implements HinweisgeberprozessDelegateLocal {

    @Inject
    HinweisgeberprozessServiceBean hinweisgeberprozessServiceBean;

    // Die Email wird aus der execution gelesen und an die ServiceBean weitergegeben.
    public void sendReceivedConfirmation(DelegateExecution execution) throws Exception {
        String email = (String) execution.getVariable("email");
        hinweisgeberprozessServiceBean.sendReceivedConfirmationToAMQ("ConfirmationOfReceipt_Queue", email);
    }

    // Die Email wird aus der execution gelesen und an die ServiceBean weitergegeben.
    public void sendTicketCanceled(DelegateExecution execution) throws Exception {
        String email = (String) execution.getVariable("email");
        hinweisgeberprozessServiceBean.sendTicketGotCanceledToAMQ("CancelTicket_Queue", email);
    }

    // Die Email wird aus der execution gelesen und an die ServiceBean weitergegeben.
    public void sendTicketCreated(DelegateExecution execution) throws Exception {
        String email = (String) execution.getVariable("email");
        hinweisgeberprozessServiceBean.sendTicketGotCreatedToAMQ("TicketGotCreated_Queue", email);
    }

    // Die ticketID und protectionMethods wird aus der execution gelesen und an die ServiceBean weitergegeben.
    public void setTicketIsExplosiveFlag(DelegateExecution execution) throws Exception {
        long ticketID = (Long) execution.getVariable("ticketID");
        String protectionMethods = (String) execution.getVariable("protectionMethods");
        hinweisgeberprozessServiceBean.sendSetTicketIsExplosiveToAMQ("UpdateIsExplosiveTicket_Queue", ticketID, protectionMethods);

    }

    // Die ticketID wird aus der execution gelesen und an die ServiceBean weitergegeben.
    public void setTicketPublicFlag(DelegateExecution execution) throws Exception {
        long ticketID = (Long) execution.getVariable("ticketID");
        hinweisgeberprozessServiceBean.sendTicketIdToAMQ("UpdateIsPublicTicket_Queue", ticketID);
    }

    // Die ticketID und problemFixed wird aus der execution gelesen und an die ServiceBean weitergegeben.
    public void createInternActionsReport(DelegateExecution execution) throws Exception {
        String problemFixed = (String) execution.getVariable("problemFixed");
        long ticketID = (Long) execution.getVariable("ticketID");
        hinweisgeberprozessServiceBean.sendCreateInternActionsReportToAMQ("CreateInternActionsReport_Queue", problemFixed, ticketID);

    }

    // Die ticketID und strategy wird aus der execution gelesen und an die ServiceBean weitergegeben.
    public void createCrisisManagementReport(DelegateExecution execution) throws Exception {

        String strategy = (String) execution.getVariable("strategy");
        long ticketID = (Long) execution.getVariable("ticketID");
        hinweisgeberprozessServiceBean.sendCreateCrisisManagementReportToAMQ("CreateCrisisManagementReport_Queue", ticketID, strategy);

    }

    // username,company,complaint,ticketID,strategy,problemFixed wird aus der execution gelesen und an die ServiceBean weitergegeben.
    public void createOverallReport(DelegateExecution execution) throws Exception {
        String username = (String) execution.getVariable("username");
        String company = (String) execution.getVariable("company");
        String complaint = (String) execution.getVariable("complaint");
        Long ticketID = (Long) execution.getVariable("ticketID");
        String strategy = (String) execution.getVariable("strategy");
        String problemFixed = (String) execution.getVariable("problemFixed");
        hinweisgeberprozessServiceBean.createOverallReportAMQ("CreateOverallReport_Queue", username, company, complaint, ticketID, strategy, problemFixed);

    }

    // username,company,complaint wird aus der execution gelesen und an die ServiceBean weitergegeben.
    // Die Methode createTicket liefert die ID des erstellten Tickets zurück welche in der execution gespeichert wird
    public void createTicketWithRest(DelegateExecution execution) throws Exception {


        String username = (String) execution.getVariable("username");
        String company = (String) execution.getVariable("company");
        String complain = (String) execution.getVariable("complaint");

        long id;
        id = hinweisgeberprozessServiceBean.createTicket(username, company, complain);

        execution.setVariable("ticketID", id);
    }

}




