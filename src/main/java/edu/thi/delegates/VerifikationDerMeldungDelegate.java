package edu.thi.delegates;


import edu.thi.beans.VerifikationDerMeldungServiceBean;
import org.camunda.bpm.engine.delegate.DelegateExecution;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

//Autor: Fabian Genes
@Stateless
@LocalBean
@Named
public class VerifikationDerMeldungDelegate implements VerifikationDerMeldungDelegateLocal {

    @Inject
    VerifikationDerMeldungServiceBean verifikationDerMeldungServiceBean;

    // Die Email und die Nachfrage wird aus der execution gelesen und an die ServiceBean weitergegeben.
    // Nach dem lesen der Nachfrage wird diese auf "LEER" gesetzt
    public void sendAdaptationRequest(DelegateExecution execution) throws Exception {
        String email = (String) execution.getVariable("email");
        String nachfragen = (String) execution.getVariable("nachfragen");
        execution.setVariable("nachfragen", "");
        verifikationDerMeldungServiceBean.sendAdaptationRequestToAMQ("AdaptationRequest_Queue", email, nachfragen);
    }

}
