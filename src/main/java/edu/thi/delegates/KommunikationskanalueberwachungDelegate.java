package edu.thi.delegates;

import edu.thi.beans.KommunikationskanalueberwachungServiceBean;
import edu.thi.entity.PublicMessage;
import org.camunda.bpm.engine.delegate.DelegateExecution;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;

@Stateless
@LocalBean
@Named

/*
 * author: Christina Huber
 * */
public class KommunikationskanalueberwachungDelegate implements KommunikationskanalueberwachungDelegateLocal {

    @Inject
    KommunikationskanalueberwachungServiceBean kommunikationskanalueberwachungServiceBean;

    // this method gets the requires message informations from camunda and gives it to the service bean.
// it gets a new messagelist and gives this to camunda over setVariable and sets the counter
    public void addMessageToList(DelegateExecution execution) throws Exception {

        long ticketId = (long) execution.getVariable("ticketID");// exists the messageMap?
        //long ticketId= 2182;
        String source = (String) execution.getVariable("source");
        String comment = (String) execution.getVariable("comment");
        String category = (String) execution.getVariable("category");
        ArrayList<PublicMessage> messageList = (ArrayList<PublicMessage>) execution.getVariable("messageList");
        ArrayList<PublicMessage> newMessageList = kommunikationskanalueberwachungServiceBean.addMessageToList(messageList, ticketId, source, category, comment);

        execution.setVariable("messageList", newMessageList);

        execution.setVariable("counter", (Integer) newMessageList.size());
    }

    public void storeMessageList(DelegateExecution execution) throws Exception {
        long ticketId = (long) execution.getVariable("ticketID");
        ArrayList<PublicMessage> messageList = (ArrayList<PublicMessage>) execution.getVariable("messageList");
        kommunikationskanalueberwachungServiceBean.storeMessageList("PublicReaction_Queue", messageList, ticketId);


    }


}




