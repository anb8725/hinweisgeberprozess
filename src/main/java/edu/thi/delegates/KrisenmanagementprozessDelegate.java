package edu.thi.delegates;

import edu.thi.beans.KrisenmanagementprozessServiceBean;
import org.camunda.bpm.engine.delegate.DelegateExecution;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;


@Stateless
@LocalBean
@Named
/*
 * author: Christina Huber
 * description: this Delegate gets variables pressStatement and category from camunda and hands over to service bean
 * */
public class KrisenmanagementprozessDelegate implements KrisenmanagementprozessDelegateLocal {

    @Inject
    KrisenmanagementprozessServiceBean krisenmanagementprozessServiceBean;

    public void sendPressStatementToPublic(DelegateExecution execution) throws Exception {

        String pressStatement = (String) execution.getVariable("pressStatment");
        long ticketID = (Long) execution.getVariable("ticketID");
        String category = (String) execution.getVariable("category"); // Automobilindustrie, Chemie, Pharma, Energie, Elektro
        krisenmanagementprozessServiceBean.sendPressStatementToPublic("Press_Queue", pressStatement, category, ticketID);

    }


}




