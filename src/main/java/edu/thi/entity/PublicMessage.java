package edu.thi.entity;

import java.io.Serializable;

//author: Christina Huber
//Description: this class builds message objects.
public class PublicMessage implements Serializable {
    private static final long serialVersionUID = 1L;
    private long ticketId;
    private String source;
    private String category;
    private String comment;
    private int messageid;

    public PublicMessage() {
        this.messageid = 0;
        this.source = null;
        this.category = null;
        this.comment = null;
    }

    public PublicMessage(long ticketId, String source, String category, String comment) {
        this.messageid = (int) Math.ceil(Math.random() * 10000);
        this.category = category;
        this.comment = comment;
        this.ticketId = ticketId;
        this.source = source;
    }

    public void setTicketId(long ticketId) {
        this.ticketId = ticketId;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getTicketId() {
        return ticketId;
    }

    public String getSource() {
        return source;
    }

    public String getCategory() {
        return category;
    }

    public String getComment() {
        return comment;
    }

    public int getMessageid() {
        return messageid;
    }
}

