# Hinweisgeber Prozess Plattform
Eine Prozess Applikation für [Camunda Platform](http://docs.camunda.org).<br/>

Dieses Projekt ist im Rahmen des Studienfaches "Implementierung von Informationssystemen" im Wintersemester 2021/2022 entstanden. 
Die Projektteilnehmer sind Anke Brenner (anb8725@thi.de), <br/> Fabian Genes(fag8777@thi.de)<br/> und Christina Huber(chh1399@thi.de) <br/>

Mittels der Camunda Process Engine  wird der Prozess einer Hinweisgeberplattform nachgebildet.<br/>

Das vorliegende Projekt besteht aus zwei getrennten Maven Projekten: <br/>


• Hinweisgeberprozess <br/>
• camel_hinweisgeberprozess <br/>


Diese Read Me bezieht sich auf beide Projekte.

## Die verwendeten Frameworks / Bestandteile

• Camunda BPMN-Engine mit WildFly Java EE Server <br/>
• Apache ActiveMQ Messaging Framework <br/>
• Apache Camel Integration Framework <br/>
• MYSQL Datenbank <br/>


## Der BPMN Hinweisgeber Prozess mit den Subprozessen
[Hinweisgeberprozess](src/main/resources/Hinweisgeberprozess.bpmn) <br/>
[Kommunikationskanalueberwachung](src/main/resources/Kommunikationskanalueberwachung.bpmn) <br/>
[Krisenmanagement](src/main/resources/Krisenmanagement.bpmn) <br/>
[Abwicklung im Unternehmen](src/main/resources/Abwicklung im Unternehmen.bpmn) <br/>
[Personenschutz einleiten](src/main/resources/Personenschutz einleiten.bpmn) <br/>
[Verifikation der Meldung](src/main/resources/Verifikation der Meldung.bpmn) <br/>

 ## Camunda Benutzer

In Camunda sind folgende Benutzer, die an der Mitwirkung des Prozesses beteiligt sind, angelegt worden:

• Personenschutzmitarbeiter
• Oeffentlichkeitsarbeiter
• Abteilungsleiter
• Finanzexperte
• Behoerdenmitarbeiter
• Polizeibeamter
• Fallmanager

### Docker Umgebung

Die Anwendung ist nur mittels Docker ausführbar. <br/>
Die Software Docker Desktop muss hierfür installiert sein.<br/>

Mittels Docker werden folgende Services unter der Verwendung von Default Ports gestartet: <br/>

• Camunda Wildfly auf Port localhost:8080 <br/>
• Apache ActiveMQ auf Port localhost:8161/admin <br/>
• MYSQL Datenbank auf Port localhost:3306 <br/>


Um die Docker Container zu starten, muss in der Konsole folgendes Kommando ausgeführt werden:<br/>

```bash
docker-compose up
```

Die benötigten Docker Dateien: <br/>
[docker-compose](docker/docker-compose.yml)<br/>
[dockerfile](docker/Dockerfile)<br/>

### Datenbank

Die in dem Projekt erzeugten Entitäten werden mittels JPA in einer MYSQL Datenbank abgespeichert. Die Java Persistence API ist in dem Projekt camel_hinweisgeberprozess lokalisiert. Für die Ausführung des Prozesses ist das Aufsetzen der Datenbank zwingend notwendig. 
Dies erfolgt automatisch durch die Ausführung des Docker Files (s."Docker Umgebung").

Zusätzliche Datenbank Informationen(vgl.docker-compose.yml):<br/>

• Datenbankname: Hinweisgeberprozess<br/>
• Benutzername: mysql<br/>
• Passwort: mysql<br/>
• Rootpasswort: rootPW<br/>

#### Maven
1.  Die Projekte müssen mittels Wildfly Maven Plugin gebaut und deployed werden ( gemäß Vorlesung "Implementierung von Informationssystemen). <br/>

Projekt hinweisgeberprozess:<br/>
```bash
mvn clean compile package wildfly:deploy
```

Projekt camel_hinweisgeberprozess <br/>

```bash
mvn clean compile camel:run
```

#### Postman Collection

Im Rahmen des Projektes wurde kein UI implementiert. Die Steuerung erfolgt im Camunda Cockpit. Postman simuliert das eigentliche Frontend. Des Weiteren wird im Subprozess "Personenschutz einleiten" für die Empfangsaufgabe die Daten mittels Postman in die Apache MQ eingegeben.<br/>

Die eingebenen POST Nachrichten werden mittels einer Postman Collection zur Verfügung gestellt: <br/>
[postman](postman/IIS_Anfragen.postman_collection.json)<br/>

